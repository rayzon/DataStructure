#include <stdio.h>
#include <stdlib.h>

typedef int ElemType;

enum NODECOLOR {RED = 0, BLACK};

typedef struct
{
    int length;
    struct Node *root;
} Header;

typedef struct Node
{
    ElemType data;
    struct Node *l_son;
    struct Node *r_son;
    struct Node *parent;
    enum NODECOLOR color;
} Node;

typedef Header* RBTree;

RBTree InitRBTree();

void Insert_RBTree(RBTree t, ElemType e);

Node* Left_Rorate(RBTree tree, Node * root);

Node* Right_Rorate(RBTree tree, Node * root);

Node* ReBalance(RBTree tree, Node* rootNode);

Node* CreateNode(ElemType e);
