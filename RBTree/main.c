#include <stdio.h>
#include <stdlib.h>
#include "main.h"

RBTree InitRBTree() {
    RBTree t = (Header*)malloc(sizeof(Header));
    t->length = 0;
    t->root = NULL;
    return t;
}

void Left_Rorate(RBTree tree, Node * cur) {
    Node* rson = cur->r_son;
    Node* parent = cur->parent;

    cur->r_son = rson->l_son;
    if (rson->l_son != NULL) {
        rson->l_son->parent = cur;
    }
    rson->parent = parent;
    if (parent == NULL) {
        tree->root = rson;
    } else if (parent->l_son == cur) {
        parent->l_son = rson;
    } else {
        parent->r_son = rson;
    }
    cur->parent = rson;
    cur->l_son = cur;
    return;
}

void Right_Rorate(RBTree tree, Node * cur) {

}

void ReBalance(RBTree tree, Node* curNode) {
    Node* parent;
    Node* grandParent;
    while (parent && parent->color == RED) {
        parent = curNode->parent;
        grandParent = parent->parent;
        if (parent == grandparent->l_son) {
            Node* uncle = grandparent->r_son;
            if (NULL != uncle && uncle->color == RED) {
                grandparent->color = RED;
                parent->color = BLACK;
                uncle->color = BLACK;
                curNode = grandparent;
                parent = curNode->parent;
            } else {
                if (curNode == parent->r_son) {
                    curNode = parent;
                    parent = curNode->parent;
                    grandParent = parent->parent;
                    Left_Rorate(tree, curNode);
                }
                grandparent->color = RED;
                parent->color = BLACK;
                Right_Rorate(tree, grandparent);
            }
        }

    }
}

Node* CreateNode(ElemType e) {
    Node* node = (Node*)malloc(sizeof(Node));
    if(NULL == node) {
        printf("����ռ�ʧ�ܣ���");
        return NULL;
    }
    node->data = e;
    node->color = RED;
    return node;
}

void Insert_RBTree(RBTree t, ElemType e){
    Node* root = t->root;
    Node* newNode = CreateNode(e);
    if (NULL == newNode) {
        return;
    }
    if (NULL == root) {
        t->root = newNode;
    } else {
        Node* cur = t->root;
        Node* parent;
        while (NULL != cur) {
            parent = cur;
            if (cur->data > e) {
                cur = cur->l_son;
            } else {
                cur = cur->r_son;
            }
        }
        if (parent->data > newNode->data) {
            parent->l_son = newNode;
        } else {
            parent->r_son = newNode;
        }
        newNode->parent = parent;
    }
    ReBalance(t, newNode);
    t->length++;
}

int main()
{
    RBTree t = InitRBTree();
    Insert_RBTree(t, 4);
    printf("%d!\n %d", t->length, t->root->color);
    return 0;
}
