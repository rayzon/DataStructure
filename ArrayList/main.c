#include <stdio.h>
#include <stdlib.h>
#include "main.h"

int main()
{
    SqList arrayList;
    InitList(&arrayList);

    ListInsert(&arrayList, 0, 12);
    ListInsert(&arrayList, 0, 17);
    ListInsert(&arrayList, 0, 20);
    ListInsert(&arrayList, 0, 10);
    ListInsert(&arrayList, 0, 123);
    ListInsert(&arrayList, 0, 155);
    ListInsert(&arrayList, 0, 109);

    AddList(&arrayList, 344);

    PrintList(arrayList);

    printf("location: %d\n", LocateElem(arrayList, 55));

    printf("deleteElem: %d\n", ListDelete(&arrayList, 3));

    PrintList(arrayList);

    Bubble_Sort_List(&arrayList);
    PrintList(arrayList);

    printf("binarysearch: %d\n", Binary_Search(arrayList.data, 344, arrayList.length));

    printf("length: %d\n", arrayList.length);
    printf("is empty? %s", ListEmpty(arrayList) ? "yes" : "no");
    return 1;
}

void InitList(SqList * L){
    L->data = (ElemType *)malloc(sizeof(ElemType)*MAXLENGTH);
    L->length = 0;
    L->now_size = MAXLENGTH;
}

int ListEmpty(SqList L){
    if (L.length <= 0){
        return 1;
    }
    return 0;
}

void ClearList(SqList L){
    L.length = 0;
}

ElemType GetElem(SqList * L, int i){
    if (i < 0 || i >= L->length || L->length == 0){
        return -1000;
    }
    return L->data[i];
}

void ListInsert(SqList * L, int i, int e){
    if (L->length >= L->now_size){
        ReSizeList(L);
    }
    int cur = L->length;
    if (i < 0 || i > L->length){
        return;
    }
    for(; cur > i; cur--){
        L->data[cur] = L->data[cur-1];
    }
    L->data[cur] = e;
    L->length ++;
    return;
}

void AddList(SqList * L, ElemType e){
    if (L->length >= L->now_size){
        ReSizeList(L);
    }
    L->data[L->length] = e;
    L->length++;
    return;
}

void ReSizeList(SqList * L){
    ElemType * data_pointer = L->data;
    ElemType * new_data = (ElemType *)malloc(L->now_size*2*sizeof(ElemType));
    for(int i = 0; i < L->length; i++){
        new_data[i] = *data_pointer;
        data_pointer++;
    }
    free(L->data);
    data_pointer = NULL;
    L->data = new_data;
    L->now_size = L->now_size*2;
    return;
}

void PrintList(SqList L){
    if (L.length <= 0){
        return;
    }
    int j = 0;
    printf("[");
    for (; j < L.length-1; j++){
        printf("%d, ", L.data[j]);
    }
    printf("%d]\n", L.data[j]);
}

int ListLength(SqList L){
    return L.length;
}

int LocateElem(SqList L, ElemType e){
    if (L.length <= 0){
        return -1;
    }
    ElemType * pointer = L.data;
    for(int i = 0; i < L.length; i++){
        if (pointer[i] == e){
            return i;
        }
    }
    return -1;
}

//二分查找，仅限于排序后的data
int Binary_Search(ElemType * data, ElemType e, int length){
    int left = 0;
    int right = length-1;
    while (left <= right){
        int mid = left + (right-left)/2;
        if (data[mid] == e){
            return mid;
        }else if(data[mid] < e){
            left = mid + 1;
        }else{
            right = mid -1;
        }
    }
    return -1;
}

int ListDelete(SqList * L, int i){
    if (i >= L->length){
        return -1000;
    }
    int deleteElem = L->data[i];
    for(int n = i; n < L->length; n++){
        L->data[n] = L->data[n+1];
    }
    L->length--;
    return deleteElem;
}

void Bubble_Sort_List(SqList * L){
    if (L->length == 0){
        return;
    }
    for (int i = 0; i < L->length; i++){
        for (int j = i+1; j < L->length; j++){
            if (L->data[i] > L->data[j]){
                L->data[i] = L->data[j] + L->data[i];
                L->data[j] = L->data[i] - L->data[j];
                L->data[i] = L->data[i] - L->data[j];
            }
        }
    }
    return;
}
