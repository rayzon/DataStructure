#include<stdio.h>
#include <stdlib.h>

#define MAXLENGTH  2
typedef int ElemType;

typedef struct {
    ElemType * data;
    int length ;
    int now_size;
} SqList;

//初始化顺序表
void InitList(SqList * L);


//半段顺序表是否为空
int ListEmpty(SqList L);

//清空顺序表
void ClearList(SqList L);


//get指定位置的值
ElemType GetElem(SqList * L, int i);

//重新设置顺序表大小
void ReSizeList(SqList * L);

//定位某值位置
int LocateElem(SqList L, ElemType e);

//指定位置插入值
void ListInsert(SqList * L, int i, int e);

//打印出列表
void PrintList(SqList L);

//顺序表末尾增加值
void AddList(SqList * L, ElemType e);

//删除指定位置的值
int ListDelete(SqList * L, int i);

//得到顺序表长度
int ListLength(SqList  L);

//二分查找
int Binary_Search(ElemType * data, ElemType e, int length);

//简单冒泡排序
void Bubble_Sort_List(SqList * L);
