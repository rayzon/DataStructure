#include <stdio.h>
#include <stdlib.h>
#include "main.h"

int main()
{
    printf("Hello world!\n");

    LinkList L = InitList();

    ListAdd(L, 32);
    ListAdd(L, 77);
    ListAdd(L, 35);
    ListAdd(L, 32);
    ListAdd(L, 876);
    PrintList(L);
    return 1;
}

LinkList InitList(){
    Header * L = (Header*)malloc(sizeof(Header));
    L->length = 0;
    L->next = NULL;
    return L;
}

void ListAdd(LinkList L, ElemType e){

    Node * NewNode = (Node*)malloc(sizeof(Node));
    NewNode->data = e;
    NewNode->next = NULL;
    L->length++;
    if(L->next == NULL){
        L->next = NewNode;
        L->length++;
        return;
    }
    Node * DataPointer = L->next;
    Node * PrePointer;
    while(DataPointer != NULL){
        PrePointer = DataPointer;
        DataPointer = DataPointer->next;
    }
    PrePointer->next = NewNode;
    L->length++;
    return;
}

void PrintList(LinkList L){
    if(L->length <= 0){
        return;
    }
    Node *cur = L->next;
    printf("[");
    while(cur != NULL){
        printf("%d, ", cur->data);
        cur = cur->next;
    }
    printf("]");
    return;
}
