#include<stdio.h>

typedef int ElemType;

typedef struct
{
    int length;
    struct Node *next;
} Header;

typedef struct Node
{
    ElemType data;
    struct Node *next;
} Node;

typedef Header* LinkList;

LinkList InitList();

void PrintList(LinkList L);

void ListAdd(LinkList L, ElemType e);
