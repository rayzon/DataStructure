#include <stdio.h>
#include <stdlib.h>

typedef int ElemType;

typedef struct {
    ElemType * dataStack;
    int length;
    int nowSize;
} MyStack;

MyStack initStack();

void push(MyStack* stack, ElemType e);

void reSize(MyStack* stack);

void printStack(MyStack stack);

ElemType pop(MyStack* stack);

ElemType stackTop(MyStack stack);

