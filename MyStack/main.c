#include <stdio.h>
#include <stdlib.h>
#include "stackImpl.h"

void inputIntStr(int* arr, int* n);
int charToInt(char c);

int main()
{
    int* arr = (int*) malloc(sizeof(int));
    int n;
    while(1) {
        inputIntStr(arr, &n);
        printf("是否为回文数：%d\n\n\n", isCircle(arr, n));
    }
    return 0;
}

void inputIntStr(int* arr, int* n) {
    char* charArr = (char*) malloc(sizeof(char));
    printf("请输入数字串长度: ");
    scanf("%d", n);
    printf("请输入数字串: ");
    scanf("%s", charArr);
    for(int i = 0; i < *n; i++) {
        arr[i] = charToInt(charArr[i]);
    }
    free(charArr);
}

int charToInt(char c) {
    return c - '0';
}
