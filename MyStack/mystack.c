#include <stdio.h>
#include <stdlib.h>
#include "mystack.h"

MyStack initStack() {
    MyStack* myStack = (MyStack*)malloc(sizeof(MyStack));
    myStack->dataStack = NULL;
    myStack->length = 0;
    myStack->nowSize = 0;
    return *myStack;
}

void push(MyStack* stack, ElemType e) {
    if (stack->dataStack == NULL) {
        stack->dataStack = (ElemType*) malloc(4 * sizeof(ElemType));
        stack->length = 4;
    } else if (stack->nowSize >= stack->length) {
        reSize(stack);
    }
    stack->dataStack[stack->nowSize] = e;
    stack->nowSize += 1;
}

ElemType pop(MyStack* stack) {
    if (NULL == stack) {
        return -9999;
    }
    if (stack->dataStack == NULL) {
        return -9999;
    }
    if (stack->nowSize <= 0){
        return -9999;
    }
    ElemType e = stack->dataStack[stack->nowSize - 1];
    stack->dataStack[stack->nowSize - 1] = 0;
    stack->nowSize--;
    return e;
}

ElemType stackTop(MyStack stack){
    if (stack.nowSize == 0) {
        return -9999;
    }
    return stack.dataStack[stack.nowSize-1];
}

void reSize(MyStack* stack) {
    if (NULL == stack){
        return;
    }
    ElemType* oldDataStack = stack->dataStack;
    ElemType* newDataStack = (ElemType*)malloc(stack->length*2*sizeof(ElemType));
    for (int i = 0; i<stack->length; i++) {
        newDataStack[i] = oldDataStack[i];
    }
    stack->length = stack->length*2;
    stack->dataStack = newDataStack;
    free(oldDataStack);
}

void printStack(MyStack stack) {
    for (int i = 0; i < stack.nowSize; i++) {
        printf("%d ", stack.dataStack[i]);
    }
}
