#include<stdio.h>
#include<stdlib.h>
#include "mystack.h"

int isCircle(int* arr, int n) {
    MyStack stack = initStack();
    for (int i = 0; i < n; i++) {
        if (stack.nowSize == 0) {
            push(&stack, arr[i]);
        } else if (stackTop(stack) == arr[i]) {
            pop(&stack);
        } else {
            push(&stack, arr[i]);
        }
    }
    return stack.nowSize == 0 ? 1 : 0;
}
